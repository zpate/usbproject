//
//  CreateThumbkeyViewController.swift
//  RegisterThumbKey
//
//  Created by Renewed Vision on 8/1/17.
//  Copyright © 2017 Renewed Vision. All rights reserved.
//

import Cocoa
import Sodium

class CreateThumbkeyViewController: NSViewController {
    
    var registrationName: String!
    var unlockCode: String!
    var sodiumObject: Sodium!
    var senderKeyPair: Box.KeyPair!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    /// Take in a string and return true if not empty
    func checkStringNotEmpty(stringToCheck: String) -> Bool {
        if stringToCheck.trimmingCharacters(in: .whitespacesAndNewlines) == "" {
            return false
        }
        return true
    }
    
    
    func encryptData(fullString: String) -> Data {
        let dataFromString = fullString.data(using: String.Encoding.utf8)!
        let encryptedMessageFromSender: Data =
            sodiumObject.box.seal(message: dataFromString, recipientPublicKey: senderKeyPair.publicKey)!
        
        return encryptedMessageFromSender
    }
    
    
    func writeEncryptedDataToFile(path: String, encryptedData: Data) {
        let filePath = path + "/RVThumbAuth.txt"
        do {
            try encryptedData.write(to: URL(fileURLWithPath: filePath))
        }
        catch let error as NSError {
            print("Error caught: \(error)")
        }
    }
    
    
    /// Creates string from registrationName, Unlock Code, and UUID and calls a function to encrypt the data
    /// It then writes the encrypted data to the
    func createAuthOnDrive(path: String, uuid: String) {
        let fullString = registrationName + " : " + unlockCode + " : " + uuid
        
        if checkStringNotEmpty(stringToCheck: registrationName) && checkStringNotEmpty(stringToCheck: unlockCode) {
            let encryptedData = encryptData(fullString: fullString)
            writeEncryptedDataToFile(path: path, encryptedData: encryptedData)
        }
    }
    
    
    @IBAction func SelectDriveForThumbkey(_ sender: NSButton) {
        // Need a way to check and make sure selected folder is a volume
        // and that it does not contain an auth file already
        let dialog = NSOpenPanel();
        dialog.title                    = "Choose a Volume";
        dialog.showsResizeIndicator     = true;
        dialog.showsHiddenFiles         = false;
        dialog.canChooseDirectories     = true;
        dialog.canCreateDirectories     = true;
        dialog.allowsMultipleSelection  = false;
        dialog.canChooseFiles           = false;
        
        if (dialog.runModal() == NSModalResponseOK) {
            let result = dialog.url
            guard let path = result?.path, let uuid = UuidOperations().getVolumeUUIDForPath(path: path) else {
                return
            }
            if !path.hasPrefix("/Volumes/") {
                print("Error: Auth info should be stored on volume")
                return
            } else {
                createAuthOnDrive(path: path, uuid: uuid)
            }
        }
    }
}
