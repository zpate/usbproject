//
//  ViewController.swift
//  RegisterThumbKey
//
//  Created by Renewed Vision on 8/1/17.
//  Copyright © 2017 Renewed Vision. All rights reserved.
//

import Cocoa
import Sodium

class ViewController: NSViewController {
    
    @IBOutlet var registrationNameField: NSTextField!
    @IBOutlet var unlockCodeField: NSTextField!
    
    let sodium = MySodium.sharedInstance.sodium
    let keyPair = MySodium.sharedInstance.sodium.box.keyPair()!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override var representedObject: Any? {
        didSet {
        // Update the view, if already loaded.
        }
    }
    
    
    func dialogOKCancel(text: String) -> Bool {
        let alert = NSAlert()
        alert.informativeText = text
        alert.alertStyle = NSAlertStyle.warning
        alert.addButton(withTitle: "Close")
        return alert.runModal() == NSAlertFirstButtonReturn
    }
    
    
    func validateChecksumOfRegistrationInfo(registrationName: String, unlockCode: String) -> Bool{
        // This is a placeholder for the RVChecksum that compares the unlock code to the user 
        // Returning true for now to assume that the name/loginkey are valid
        return true
    }
    
    
    override func prepare(for segue: NSStoryboardSegue, sender: Any?) {
        if segue.identifier == "validateCredentialsSegue" {
            let toVC = segue.destinationController as! CreateThumbkeyViewController
            toVC.registrationName = registrationNameField.stringValue
            toVC.unlockCode = unlockCodeField.stringValue
            toVC.sodiumObject = sodium
            toVC.senderKeyPair = keyPair
        }
        
        if segue.identifier == "existingThumbkeySegue" {
            let toVC = segue.destinationController as! ExistingThumbkeyViewController
            toVC.sodiumObject = sodium
            toVC.senderKeyPair = keyPair
        }
    }
    
    
    @IBAction func validateCredentials(_ sender: Any) {
        if validateChecksumOfRegistrationInfo(registrationName: registrationNameField.stringValue, unlockCode: unlockCodeField.stringValue) {
            performSegue(withIdentifier: "validateCredentialsSegue", sender: NSButton())
        } else {
            dialogOKCancel(text: "Please enter a valid Registration Name and Unlock Code")
        }
    }
    
    
    @IBAction func segueToExistingVC(_ sender: NSButton) {
        performSegue(withIdentifier: "existingThumbkeySegue", sender: NSButton())
    }
    
}
