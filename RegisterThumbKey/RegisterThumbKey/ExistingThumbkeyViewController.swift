//
//  ExistingThumbkeyViewController.swift
//  RegisterThumbKey
//
//  Created by Renewed Vision on 8/2/17.
//  Copyright © 2017 Renewed Vision. All rights reserved.
//

import Cocoa
import Sodium

class ExistingThumbkeyViewController: NSViewController {
    
    var sodiumObject: Sodium!
    var senderKeyPair: Box.KeyPair!

    override func viewDidLoad() {
        super.viewDidLoad()
        // print("Existing PK: ", senderKeyPair.publicKey.base64EncodedString())
        validateLicenseIfHasThumbkeyAuth()
    }
    
    
    enum thumbkeyCheckError: Error {
        case noAuthFile
    }
    
    
    func validateLicenseIfHasThumbkeyAuth() {
        do {
            let filePath = try iterateVolumesAndFindFileIfExists()
            let path = filePath.replacingOccurrences(of: "/RVThumbAuth.txt", with: "")
            let decryptedString = readAndDecryptAuthFile(filePath: filePath)
            if splitStringAndGetInfo(decryptedString: decryptedString, path: path) {
                print("User auth Success")
            }
        }
        catch let error as NSError {
            print(error)
        }
    }
    
    
    /// Iterate through volumes and find the RVThumbAuth.txt file if it exists
    /// Returns the path to the auth.txt file
    /// If !exists, redirect to root and alert
    func iterateVolumesAndFindFileIfExists() throws -> String {
        let fileManager = FileManager()
        let en = fileManager.enumerator(atPath: "/Volumes")
        
        while let element = en?.nextObject() as? String {
            if element.contains("RVThumbAuth.txt") {
                return "/Volumes/" + element
            }
        }
        throw thumbkeyCheckError.noAuthFile
    }
    
    
    /// takes in a filePath, reads the data and decrypts using the key
    func readAndDecryptAuthFile(filePath: String) -> String {
        do {
            let encryptedData = try Data(contentsOf: URL(fileURLWithPath: filePath))
            let decryptedData =
                sodiumObject.box.open(anonymousCipherText: encryptedData,
                                      recipientPublicKey: senderKeyPair.publicKey,
                                      recipientSecretKey: senderKeyPair.secretKey)
            
            let decryptedString = String(data: decryptedData!, encoding: .utf8)
            return decryptedString!
            
        } catch let error as NSError {
            print(error)
            return ""
        }
    }
    
    
    /// Because the file contains three pieces of information separated by colons, 
    /// take in the decrypted string and split it into three different pieces of information
    func splitStringAndGetInfo(decryptedString: String, path: String) -> Bool {
        let decryptedStringArray = decryptedString.components(separatedBy: " : ")
        if validateChecksumOfUnlockAndRegisteredTo(registeredTo: decryptedStringArray[0], unlockCode: decryptedStringArray[1]) && validateUuidMatchesDrive(uuid: decryptedStringArray[2], path: path){
            return true
        }
        return false
    }
    
    
    func validateChecksumOfUnlockAndRegisteredTo(registeredTo: String, unlockCode: String) -> Bool {
        print("registered To: \(registeredTo)")
        print("Unlock Code \(unlockCode)")
        let file = NSURL(fileURLWithPath: "/Volumes/External")
        
        let uuid = kCFURLVolumeUUIDStringKey
        print(uuid)
        return true
    }
    
    
    func validateUuidMatchesDrive(uuid: String, path: String) -> Bool {
        if (uuid != UuidOperations().getVolumeUUIDForPath(path: path)) {
            print("UUID does not match")
            return false
        }
        print("UUID: \(uuid)")
        return true
    }
    
    
    
}
