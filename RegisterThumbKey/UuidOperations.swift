//
//  UuidOperations.swift
//  RegisterThumbKey
//
//  Created by Renewed Vision on 8/14/17.
//  Copyright © 2017 Renewed Vision. All rights reserved.
//

import Foundation

class UuidOperations {
    func getVolumeUUIDForPath(path: String) -> String? {
        var retval: String?
        
        if let session = DASessionCreate(kCFAllocatorDefault)
        {
            var fsStats = UnsafeMutablePointer<statfs>.allocate(capacity: 1)
            defer { fsStats.deinitialize(count: 1) }
            if statfs((path as NSString).utf8String, fsStats) == 0
            {
                let deviceName = withUnsafePointer(to: &fsStats.pointee.f_mntfromname, {
                    (ptr) -> String? in let int8Ptr = unsafeBitCast(ptr, to: UnsafePointer<Int8>.self)
                    return String.init(validatingUTF8: int8Ptr)
                })
                if let device = deviceName,
                    let disk = DADiskCreateFromBSDName(kCFAllocatorDefault, session, device),
                    let description = DADiskCopyDescription(disk)
                {
                    let myDict = description as NSDictionary
                    let uuid = myDict[kDADiskDescriptionVolumeUUIDKey as String /* "DAVolumeUUIDD" */] as! CFUUID
                    retval = CFUUIDCreateString(kCFAllocatorDefault, uuid) as String
                }
                else { retval = "" }
            }
        }
        return retval
    }
}
