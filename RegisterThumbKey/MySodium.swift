//
//  MySodium.swift
//  RegisterThumbKey
//
//  Created by Renewed Vision on 8/9/17.
//  Copyright © 2017 Renewed Vision. All rights reserved.
//

import Foundation
import Sodium

class MySodium {
    static var sharedInstance = MySodium()
    
    let sodium = Sodium()!
}
