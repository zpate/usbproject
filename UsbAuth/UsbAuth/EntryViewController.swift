//
//  EntryViewController.swift
//  UsbAuth
//
//  Created by Renewed Vision on 7/27/17.
//  Copyright © 2017 Renewed Vision. All rights reserved.
//

import Cocoa

class EntryViewController: NSViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        var Zach = User(email: "zach@email.com", unlockCode: "aabb-ccdd-eeff-gghh-iijj-kkll-mmnn", hasThumbkeyAuth: true, thumbkeyPath: "/Volumes/Samsung USB", thumbkeyUUID: "D47A71AB-D0EC-3A8F-BA10-7920637DFD59", hashKey: "d090d3ae5b50ec31b0176fdbd8e4e799d4d7e98320e9d102623bef6c742b3831")
        var Nick = User(email: "Nick@email.com", unlockCode: "abcd-efgh-ijkl-mnop-qrst-uvwx-yyzz", hasThumbkeyAuth: false, thumbkeyPath: nil, thumbkeyUUID: nil, hashKey: nil)
        
        CurrentUser.sharedInstance.user = Nick
    }
    
    
    @IBAction func registerThumbkey(_ sender: NSButton) {
        if CurrentUser.sharedInstance.user?.hasThumbkeyAuth == false {
            performSegue(withIdentifier: "registerThumbkeySegue", sender: NSButton())
        } else {
            print("User already has Thumbkey. Cannot register a new one")
        }
    }
    
    
    @IBAction func signInWithExistingthumbkey(_ sender: NSButton) {
        if CurrentUser.sharedInstance.user?.hasThumbkeyAuth == true {
            performSegue(withIdentifier: "existingThumbkeySegue", sender: NSButton())
        } else {
            print("User does not have thumbkey. Register thumbkey first to access login")
        }
    }
    
}
