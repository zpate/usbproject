//
//  ExistingThumbkeyViewController.swift
//  UsbAuth
//
//  Created by Renewed Vision on 7/27/17.
//  Copyright © 2017 Renewed Vision. All rights reserved.
//

import Cocoa
import DiskArbitration

class ExistingThumbkeyViewController: NSViewController {
    @IBOutlet var successOrFailureField: NSTextField!
    
    // These represent the values that will need to be pulled from the user's info
    let registeredTo = CurrentUser.sharedInstance.user!.email
    let thumbkeyUuid = CurrentUser.sharedInstance.user!.thumbkeyUUID!
    let thumbKeyPath = CurrentUser.sharedInstance.user!.thumbkeyPath!
    let hashKey = CurrentUser.sharedInstance.user?.hashKey!

    override func viewDidLoad() {
        super.viewDidLoad()
        testEverything()
    }
    
    
    /// Takes in a path and returns the UUID if it exists. Returns "" if the path does not contain a UUID
    func getVolumeUUIDForPath(path: String) -> String? {
        var retval: String?
        
        if let session = DASessionCreate(kCFAllocatorDefault)
        {
            var fsStats = UnsafeMutablePointer<statfs>.allocate(capacity: 1)
            defer { fsStats.deinitialize(count: 1) }
            if statfs((path as NSString).utf8String, fsStats) == 0
            {
                let deviceName = withUnsafePointer(to: &fsStats.pointee.f_mntfromname, {
                    (ptr) -> String? in let int8Ptr = unsafeBitCast(ptr, to: UnsafePointer<Int8>.self)
                    return String.init(validatingUTF8: int8Ptr)
                })
                if let device = deviceName,
                    let disk = DADiskCreateFromBSDName(kCFAllocatorDefault, session, device),
                    let description = DADiskCopyDescription(disk)
                {
                    let myDict = description as NSDictionary
                    let uuid = myDict[kDADiskDescriptionVolumeUUIDKey as String /* "DAVolumeUUIDD" */] as! CFUUID
                    retval = CFUUIDCreateString(kCFAllocatorDefault, uuid) as String
                }
                else { retval = "" }
            }
        }
        return retval
    }
    
    
    /// Takes a path and uuid. First checks to make sure the path exists and then 
    /// checks that the path has a UUID and that the UUID matches the user's thumbkey UUID
    func checkThumbkeyPathExistsAndPathHasCorrectUUID(path: String, uuid: String) -> Bool {
        let fileManager = FileManager.default
        if !fileManager.fileExists(atPath: path) {
            print("No volume at path")
            successOrFailureField.stringValue = "No Volume at Path \(path)"
            successOrFailureField.textColor = NSColor.red
            return false
        }
        if getVolumeUUIDForPath(path: path) == "" || getVolumeUUIDForPath(path: path) != uuid {
            print("Path does not have UUID or does not match given UUID")
            successOrFailureField.stringValue = "Path does not have UUID or does not match given UUID"
            successOrFailureField.textColor = NSColor.red
            return false
        }
        print("checkThumbkeyPathExistsAndPathHasCorrectUUID() Works")
        return true
    }
    
    
    /// Takes in a path (to the volume used as a thumbkey) and ensures that it has an "Auth.txt" file
    /// It then checks the file's content (should be the hashed unlock code) and makes sure that it is equal to the 
    /// unlock code registered to the user
    func checkIfCredentialFileExistsAndAuthenticate(path: String) -> Bool {
        let url = NSURL(fileURLWithPath: path)
        let filePath = url.appendingPathComponent("auth.txt")?.path
        let fileManager = FileManager.default
        if !fileManager.fileExists(atPath: filePath!) {
            print("No Auth file at path")
            return false
        }
        
        do {
            let data = try NSString(contentsOfFile: filePath!, encoding: String.Encoding.ascii.rawValue)
            if data as String == hashKey {
                print("User authenticated with thumbkey")
                successOrFailureField.stringValue = "User authenticated with thumbkey at Path \(path)"
                successOrFailureField.textColor = NSColor.green
                return true
            }
        } catch {
            print("File not able to be read")
            return false
        }
      return false
    }
    
    
    func testEverything() {
        
        if checkThumbkeyPathExistsAndPathHasCorrectUUID(path: thumbKeyPath, uuid: thumbkeyUuid) && checkIfCredentialFileExistsAndAuthenticate(path: thumbKeyPath) {
            print("User authenticated")
        }
    }

    
}
