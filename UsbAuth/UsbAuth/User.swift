//
//  User.swift
//  UsbAuth
//
//  Created by Renewed Vision on 7/31/17.
//  Copyright © 2017 Renewed Vision. All rights reserved.
//

import Foundation

/// Basic user class 
/// Optional properties will stay 'nil' if user does not have thumbkey auth
public class User {
    var email: String
    var unlockCode: String
    var hasThumbkeyAuth: Bool
    var thumbkeyPath: String?
    var thumbkeyUUID: String?
    var hashKey: String?
    var users = [User]()
    
    
    init(email: String, unlockCode: String, hasThumbkeyAuth: Bool, thumbkeyPath: String?, thumbkeyUUID: String?, hashKey: String?) {
        self.email = email
        self.unlockCode = unlockCode
        self.hasThumbkeyAuth = hasThumbkeyAuth
        self.thumbkeyPath = thumbkeyPath
        self.thumbkeyUUID = thumbkeyUUID
        self.hashKey = hashKey
        
        users.append(self)
    }
    
    func printArray() {
        for u in users {
            print(u)
        }
    }
}
