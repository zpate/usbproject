//
//  CurrentUser.swift
//  UsbAuth
//
//  Created by Renewed Vision on 7/31/17.
//  Copyright © 2017 Renewed Vision. All rights reserved.
//

import Foundation

/// Singleton of User type to simplify access/mutation of user
class CurrentUser {
    static var sharedInstance = CurrentUser()
    var user: User?
}
