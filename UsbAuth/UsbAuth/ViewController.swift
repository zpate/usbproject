//
//  ViewController.swift
//  UsbAuth
//
//  Created by Renewed Vision on 7/27/17.
//  Copyright © 2017 Renewed Vision. All rights reserved.
//

import Cocoa
import DiskArbitration

class ViewController: NSViewController {
    @IBOutlet var filenameLabel: NSTextField!

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override var representedObject: Any? {
        didSet {        }
    }
    
    
    /// Takes in a path and returns the volume UUID for the device located at the path
    func getVolumeUUIDForPath(path: String) -> String?
    {
        var retval: String?
        
        if let session = DASessionCreate(kCFAllocatorDefault)
        {
            var fsStats = UnsafeMutablePointer<statfs>.allocate(capacity: 1)
            defer { fsStats.deinitialize(count: 1) }
            if statfs((path as NSString).utf8String, fsStats) == 0
            {
                let deviceName = withUnsafePointer(to: &fsStats.pointee.f_mntfromname, {
                    (ptr) -> String? in let int8Ptr = unsafeBitCast(ptr, to: UnsafePointer<Int8>.self)
                    return String.init(validatingUTF8: int8Ptr)
                })
                if let device = deviceName,
                    let disk = DADiskCreateFromBSDName(kCFAllocatorDefault, session, device),
                    let description = DADiskCopyDescription(disk)
                {
                    let myDict = description as NSDictionary
                    let uuid = myDict[kDADiskDescriptionVolumeUUIDKey as String /* "DAVolumeUUIDD" */] as! CFUUID
                    retval = CFUUIDCreateString(kCFAllocatorDefault, uuid) as String
                }
                else { retval = "" }
            }
        }
        return retval
    }
    
    
    /// Use CommonCrypto to vonvert unlock code to SHA-256 hash
    func hashAuthInfo(registeredTo: String, authKey: String) -> String {
        guard let messageData = authKey.data(using:String.Encoding.utf8) else { return "error"}
        var digestData = Data(count: Int(CC_SHA256_DIGEST_LENGTH))
        _ = digestData.withUnsafeMutableBytes {digestBytes in
            messageData.withUnsafeBytes {messageBytes in
                CC_SHA256(messageBytes, CC_LONG(messageData.count), digestBytes)
            }
        }
        let hexData = digestData.map { String(format: "%02hhx", $0) }.joined()
        return hexData
    }
    
    
    /// Takes in a path and a hex representation of the user's auth key and writes it 
    /// to the file at the given path
    /// If successful, updates current user with thumbkey auth properties
    func writeFileToPath(path: String, encryptedAuthKey: String, uuid: String) {
        let filePath = path + "/auth.txt"
        do {
            try encryptedAuthKey.write(toFile: filePath, atomically: false, encoding: .utf8)
            performSegue(withIdentifier: "thumbkeyRegisterSuccess", sender: NSButton())
            CurrentUser.sharedInstance.user!.hasThumbkeyAuth = true
            CurrentUser.sharedInstance.user!.thumbkeyPath = path
            CurrentUser.sharedInstance.user!.thumbkeyUUID = uuid
            CurrentUser.sharedInstance.user!.hashKey = encryptedAuthKey
        }
        catch let error as NSError {
            print("Error caught: \(error)")
        }
    }
    
    
    /// Allows a user to choose a disk that will hold their authentication credentials
    /// It then calls getVolumeUUIDForPath() to get the uuid of the chosen drive.
    /// The function then sends the user authkey to hashAuthInfo() and hashes the user authKey (with SHA-256).
    /// After this, it calls writeFileToPath to write the hashed string (hashed authKey) to the 
    /// auth.txt file in the selected drive.
    @IBAction func browseFiles(_ sender: NSButton) {
        let dialog = NSOpenPanel();
        dialog.title                  = "Choose a Volume";
        dialog.showsResizeIndicator     = true;
        dialog.showsHiddenFiles        = false;
        dialog.canChooseDirectories     = true;
        dialog.canCreateDirectories     = true;
        dialog.allowsMultipleSelection  = false;
        dialog.canChooseFiles          = false;
        
        if (dialog.runModal() == NSModalResponseOK) {
            let result = dialog.url
            guard let path = result?.path, let uuid = getVolumeUUIDForPath(path: path) else {
                return
            }
            
            filenameLabel.stringValue = path
            let hexAuthKey = hashAuthInfo(registeredTo: CurrentUser.sharedInstance.user!.email, authKey: CurrentUser.sharedInstance.user!.unlockCode)
            writeFileToPath(path: path, encryptedAuthKey: hexAuthKey, uuid: uuid)
        }
    }
}

